'use strict';

const { migrator, types } = require('@samsch/smart-migrations');
const ref = require('../migrations-util/reference');

module.exports = migrator([
	{
		tables: [],
		up: async knex => {
			await knex.raw(`
CREATE FUNCTION raise_exception() RETURNS trigger AS $$
BEGIN
	RAISE EXCEPTION 'May not update created_at timestamps - on table %', TG_TABLE_NAME;
END;
$$ LANGUAGE plpgsql;`);
		},
		down: async knex => {
			await knex.raw('DROP FUNCTION raise_exception;');
		},
	},
	{
		tables: 'users',
		easyTable: {
			id: types.id_int,
			email: 'text',
			name: 'text',
			password: 'text',
			...types.timestamps(),
		},
	},
	{
		tables: 'sessions',
		easyTable: {
			sid: 'text|primary',
			sess: 'json',
			expired: 'timestamp|index',
		},
	},
	{
		tables: 'foods',
		easyTable: {
			id: types.id_int,
			name: 'text',
			calories: 'integer',
			last_used: 'timestamp|nullable',
		},
	},
	{
		tables: 'users_foods',
		easyTable: {
			id: types.id_int,
			user_id: ref('users'),
			name: 'text|nullable',
			calories: 'integer|nullable',
			food_id: ref('foods', { nullable: true }),
			eaten_at: 'timestamp',
		},
	},
]);
