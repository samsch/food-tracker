'use strict';

function ref(
	table,
	{
		index = true,
		nullable = false,
		referenceColumn = 'id',
		onDelete = null,
		onUpdate = null,
	} = {},
) {
	return [
		'integer',
		`references:${table}.${referenceColumn}`,
		index ? 'index' : [],
		nullable ? 'nullable' : [],
		onDelete ? `onDelete:${onDelete}` : [],
		onUpdate ? `onUpdate:${onUpdate}` : [],
	]
		.flat()
		.join('|');
}

module.exports = ref;
