'use strict';

const routerFactory = require('express-promise-router');

module.exports = function useRouter(app, definer) {
	const router = routerFactory();
	definer(router);
	app.use(router);
};
