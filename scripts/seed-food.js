'use strict';

process.env.NODE_ENV = 'development';

const knexFactory = require('knex');
const Promise = require('bluebird');
const faker = require('faker');

const knex = knexFactory(require('../knexfile'));

const foods = [
	['rice', 450],
	['egg over medium with cheese', 450],
	['bagel', 450],
	['oreos 2', 450],
	['nutter butter 2', 450],
	['carrots 2/3 lb', 450],
	['broccoli', 450],
];

Promise.try(() => {
	const randomfoods = Array.from({ length: 20 }, (_, i) => i).map((_, i) => {
		const aFood = faker.random.arrayElement(foods);
		return {
			user_id: 1,
			name: aFood[0],
			calories: aFood[1],
			eaten_at: knex.raw(`now() - interval '${i * 3} hours'`),
		};
	});
	return knex('users_foods').insert(randomfoods);
})
	.catch(error => {
		console.log(error);
	})
	.finally(() => {
		knex.destroy();
	});
