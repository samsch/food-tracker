CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
create database se_testing;
create user se_testing with encrypted password 'se_testing';
grant all privileges on database se_testing to se_testing;
