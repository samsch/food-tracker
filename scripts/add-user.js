'use strict';

process.env.NODE_ENV = 'development';

const knexFactory = require('knex');
const Promise = require('bluebird');
const argon2 = require('argon2');

function makePasswordHash(password) {
	return argon2.hash(password);
}

const knex = knexFactory(require('../knexfile'));

Promise.try(() => {
	return makePasswordHash('sass');
})
	.then(password => {
		return knex('users').insert({
			name: 'Sam Scheiderich',
			email: 'sam@samsch.org',
			password,
		});
	})
	.catch(error => {
		console.log(error);
	})
	.finally(() => {
		process.exit();
	});
