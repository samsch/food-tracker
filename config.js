'use strict';

const assert = require('assert').strict;
const config = require('./config.json');

module.exports = {
	isDev() {
		return config.env === 'development';
	},
	isProd() {
		return (config.env ?? 'production') === 'production';
	},
	sessionSecret:
		config.sessionSecret ??
		assert(
			config.sessionSecret,
			'sessionSecret must be set to long random string',
		),
};
