'use strict';

const argon2 = require('argon2');
const makeLoginRoute = require('./make-login-route');

module.exports = function (deps) {
	return makeLoginRoute({
		getCredentialsFromRequest: req => ({
			login: req.body.email,
			password: req.body.password,
		}),
		getUserByLogin: login =>
			deps
				.knex('users')
				.select(['id', 'email', 'password'])
				.where('email', login)
				.first(),
		verifyPassword: (hash, password) => argon2.verify(hash, password),
		getSafeAuthUser: user =>
			deps.knex('users').select(['id', 'name']).where('id', user.id),
		// eslint-disable-next-line no-unused-vars
		sendSuccessResponse: (user, req, res, next) => {
			req.flash('message', 'Logged in!');
			res.redirect(303, '/');
		},
		// eslint-disable-next-line no-unused-vars
		sendValidationError: (validationError, req, res, next) => {
			req.flash('form-error', `Invalid Input: ${validationError.errors}`);
			res.redirect(303, '/login');
		},
		// eslint-disable-next-line no-unused-vars
		sendAuthenticationError: (req, res, next) => {
			req.flash('form-error', 'Email and Password combination incorrect');
			res.redirect(303, '/login');
		},
	});
};
