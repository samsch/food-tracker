'use strict';

const Promise = require('bluebird');
const { validateValue, AggregrateValidationError } = require('@validatem/core');
const isString = require('@validatem/is-string');
const required = require('@validatem/required');
const isFunction = require('@validatem/is-function');
const { loginUser, logout } = require('@samsch/use-authentication');

class IncorrectLoginCredentials extends Error {}

function makeLoginRoute(args) {
	const {
		getCredentialsFromRequest,
		getUserByLogin,
		verifyPassword,
		getSafeAuthUser,
		sendSuccessResponse,
		sendValidationError,
		sendAuthenticationError,
	} = validateValue(args, {
		getCredentialsFromRequest: [required, isFunction],
		getUserByLogin: [required, isFunction],
		verifyPassword: [required, isFunction],
		getSafeAuthUser: [required, isFunction],
		sendSuccessResponse: [required, isFunction],
		sendValidationError: [required, isFunction],
		sendAuthenticationError: [required, isFunction],
	});
	return function loginRoute(req, res, next) {
		return Promise.try(async () => {
			const { login, password } = validateValue(
				getCredentialsFromRequest(req),
				{
					login: [required, isString],
					password: [required, isString],
				},
			);
			const userWithPassword = await getUserByLogin(login);
			if (!userWithPassword) {
				throw new IncorrectLoginCredentials();
			}
			const isPasswordCorrect = await verifyPassword(
				userWithPassword.password,
				password,
			);
			if (!isPasswordCorrect) {
				throw new IncorrectLoginCredentials();
			}
			const [user] = await Promise.all([
				getSafeAuthUser(userWithPassword),
				loginUser(req, userWithPassword),
			]);
			req.user = user;
			return sendSuccessResponse(user, req, res, next);
		})
			.catch(AggregrateValidationError, validationError => {
				return sendValidationError(validationError, req, res, next);
			})
			.catch(IncorrectLoginCredentials, async () => {
				await logout(req);
				return sendAuthenticationError(req, res, next);
			});
	};
}

module.exports = makeLoginRoute;
