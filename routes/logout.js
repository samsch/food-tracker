'use strict';

const Promise = require('bluebird');
const { logout } = require('@samsch/use-authentication');

// eslint-disable-next-line no-unused-vars
module.exports = function (deps) {
	return function loginRoute(req, res) {
		return Promise.try(() => {
			return logout(req);
		}).then(() => {
			req.flash('messages', 'Logged out!');
			res.status(303).redirect('/');
		});
	};
};
