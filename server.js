'use strict';

const path = require('path');
const express = require('express');
const knexFactory = require('knex');
const helmet = require('helmet');

const useKnexSessions = require('@samsch/use-knex-sessions');
const useCSRFProtection = require('@samsch/use-csrf-protection');
const useAuthentication = require('@samsch/use-authentication');
const useErrorHandler = require('@samsch/use-error-handler');
const { NotFoundHTTPException } = require('@samsch/http-errors');
const createRenderer = require('@nsrv/express-jsx-views');

const useRouter = require('./use-router');

const { isProd, sessionSecret } = require('./config');
const { requireAuthentication } = require('./authentication-middleware');

const knex = knexFactory(require('./knexfile'));

const app = express();

app.use((req, res, next) => {
	res.locals.req = req;
	next();
});

if (isProd()) {
	app.use(helmet());
	app.set('trust proxy', 'loopback');
}

if (!isProd) {
	// nginx will serve static files in production
	app.use(express.static(path.resolve(__dirname, 'static')));
}
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.set('views', path.resolve(__dirname, 'views'));
app.set('view engine', 'jsx');
app.engine('jsx', createRenderer('inferno'));

useKnexSessions(app, knex, {
	secret: sessionSecret,
	rolling: true,
	cookie: {
		maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week in milliseconds
		secure: isProd(),
	},
});

app.use((req, res, next) => {
	req.flash = function (type, newMessage) {
		if (!req.session.flashes) {
			req.session.flashes = {};
		}
		if (newMessage != null) {
			if (!req.session.flashes[type]) {
				req.session.flashes[type] = [];
			}
			req.session.flashes[type].push(newMessage);
			return req.session.flashes[type];
		}
		const flashes = req.session.flashes[type] ?? [];
		req.session.flashes[type] = [];
		return flashes;
	};
	next();
});

useCSRFProtection(app);

useAuthentication(app, {
	// eslint-disable-next-line no-shadow
	fetchUser(id) {
		return knex('users').select(['id', 'name']).where('id', id).first();
	},
});

useRouter(app, router => {
	router.get('/login', (req, res) => {
		res.render('login.jsx');
	});

	router.post('/login', require('./routes/login')({ knex }));

	router.get('/', requireAuthentication, async (req, res) => {
		const data = await require('./models/home-data')(req.user, { knex });
		res.render('home.jsx', data);
	});

	router.post('/eat-food', requireAuthentication, async (req, res) => {
		require('./models/eat-a-food')(req.user, req.body, { knex });
		req.flash('message', 'Added eaten food');
		res.redirect(303, '/');
	});

	router.post('/delete-user-food', requireAuthentication, async (req, res) => {
		await knex('users_foods')
			.where({ user_id: req.user.id, id: req.body.value })
			.delete();
		req.flash('message', 'Deleted eaten food');
		res.redirect(303, '/');
	});

	router.post(
		'/duplicate-user-food',
		requireAuthentication,
		async (req, res) => {
			const food = await knex('users_foods')
				.where({
					user_id: req.user.id,
					id: req.body.value,
				})
				.first();
			if (!food) {
				throw new NotFoundHTTPException();
			}
			await knex('users_foods').insert({
				...Object.fromEntries(
					Object.entries(food).filter(([key]) => key !== 'id'),
				),
				eaten_at: knex.raw('now()'),
			});
			req.flash('message', 'Added eaten food');
			res.redirect(303, '/');
		},
	);

	router.post('/add-food-preset', requireAuthentication, async (req, res) => {
		require('./models/add-a-food')(req.body, { knex });
		req.flash('message', 'Added food');
		res.redirect(303, '/');
	});

	router.post('/eat-preset-food', requireAuthentication, async (req, res) => {
		await knex.transaction(async trx => {
			const food = await trx('foods')
				.where({ id: Number(req.body.value) })
				.first();
			if (!food) {
				throw new NotFoundHTTPException();
			}
			await trx('users_foods').insert({
				user_id: req.user.id,
				name: food.name,
				calories: food.calories,
				food_id: food.id,
				eaten_at: knex.raw('now()'),
			});
			await trx.raw(
				`
update "foods"
	set
		last_used = now()
	where "id" = ?
			`,
				[food.id],
			);
			req.flash('message', 'Added eaten food');
			res.redirect(303, '/');
		});
	});

	router.post('/api/logout', require('./routes/logout')({ knex }));

	router.get('/api/user', requireAuthentication, (req, res) => {
		res.json(req.user);
	});

	router.get('*', () => {
		throw new NotFoundHTTPException();
	});
});

useErrorHandler(app, {
	debug: true,
	json: output => {
		console.error(output);
		return output;
	},
	500: 'errors/500.jsx',
});

if (isProd) {
	app.listen(3000, 'localhost', () => {
		console.log('Listening at http://localhost:3000');
	});
} else {
	app.listen(3000, () => {
		console.log('Listening at http://localhost:3000');
	});
}
