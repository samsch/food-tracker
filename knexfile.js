'use strict';

module.exports = {
	client: 'pg',
	connection: {
		...require('./config.json').knex,
	},
	migrations: {
		stub: require('@samsch/smart-migrations').getDefaultStubPath(),
	},
};
