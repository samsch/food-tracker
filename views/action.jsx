'use strict';

const CsrfField = require('./csrf-field');

const Action = ({ route, value, label }) => {
	if (!route || !value || !label) {
		throw new Error(
			`All props required: { route, value, label }, received: ${JSON.stringify({
				route,
				value,
				label,
			})}`,
		);
	}
	return (
		<form action={route} method="post">
			<CsrfField />
			<input type="hidden" name="value" value={value} />
			<button type="submit">{label}</button>
		</form>
	);
};

module.exports = Action;
