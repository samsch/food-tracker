'use strict';

module.exports = function Nav(_, { _locals }) {
	const req = _locals.req;
	const user = req.user;
	return (
		<nav>
			<ul className="flex">
				<li>
					<a className="text-blue-600" href="/">
						Home
					</a>
				</li>
				{user ? (
					<li>
						<form action="/api/logout" method="post">
							<input type="hidden" name="csrf_token" value={req.csrfToken()} />
							<button type="submit">Logout</button>
						</form>
					</li>
				) : (
					<li>
						<a className="text-blue-600" href="/login">
							Login
						</a>
					</li>
				)}
			</ul>
		</nav>
	);
};
