'use strict';

const Html = require('../html');

module.exports = function E500({ status, code }) {
	return (
		<Html title="Error 500 - Simple Expected">
			<h1>Error 500 page</h1>
			<p>For real, code: {code}</p>
			<p>Which probably means: {status}</p>
		</Html>
	);
};
