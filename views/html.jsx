'use strict';

function Html(
	// eslint-disable-next-line react/prop-types
	{ title, children, metaViewPort, metaCharset, headChildren },
	{ _locals },
) {
	if (!children) {
		throw new Error('DefaultLayout expects truthy `children` prop!');
	}
	if (!_locals) {
		throw new Error('DefaultLayout expects truthy `_locals` prop!');
	}
	// eslint-disable-next-line react/prop-types
	if (typeof title !== 'string' || title.length === 0) {
		throw new Error('DefaultLayout expects non-empty string `title` prop.');
	}
	const messages = _locals.req
		.flash('messages')
		.concat(_locals.req.flash('message'));
	const messageAlert = messages.length ? (
		<ul className="fixed z-10 top-5 w-auto bg-white rounded border-2 border-indigo-400">
			{messages.map(message => {
				return <li>{message}</li>;
			})}
		</ul>
	) : null;
	return (
		<html lang="en">
			<head>
				{metaCharset ?? <meta charSet="UTF-8" />}
				{metaViewPort ?? (
					<meta
						name="viewport"
						content="width=device-width, initial-scale=1.0"
					/>
				)}
				<title>{title}</title>
				{/* <link
					href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css"
					rel="stylesheet"
				/> */}
				{headChildren}
			</head>
			<body>
				{messageAlert}
				<div className="container mx-auto">{children}</div>
			</body>
		</html>
	);
}

module.exports = Html;
