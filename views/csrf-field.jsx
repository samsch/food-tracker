'use strict';

const CsrfField = (props, { _locals }) => {
	return (
		<input type="hidden" name="csrf_token" value={_locals.req.csrfToken()} />
	);
};
module.exports = CsrfField;
