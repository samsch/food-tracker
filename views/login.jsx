'use strict';

const Html = require('./html');
const Nav = require('./nav');

module.exports = function Login({ _locals }) {
	const { user } = _locals;
	return (
		<Html title="Login - Food Tracker">
			<h1 className="mt-4 text-4xl font-bold">Login Page</h1>
			<Nav />
			{user ? (
				<p className="mt-4">
					Already logged in as {user.name}. Click Logout above to log out.
				</p>
			) : (
				<div>
					{(() => {
						const errors = _locals.req.flash('form-error');
						if (errors.length === 0) {
							return null;
						}
						return (
							<>
								<h2>Form Errors</h2>
								<ul>
									{errors.map(error => {
										return <li>{error}</li>;
									})}
								</ul>
							</>
						);
					})()}
					<h2>Login Form</h2>
					<form method="post" action="/login">
						<input
							type="hidden"
							name="csrf_token"
							value={_locals.req.csrfToken()}
						/>
						<div>
							<label htmlFor="email">Email</label>
							<input type="text" id="email" name="email" />
						</div>
						<div>
							<label htmlFor="password">Password</label>
							<input type="password" id="password" name="password" />
						</div>
						<div>
							<button type="submit">Log in</button>
						</div>
					</form>
				</div>
			)}
		</Html>
	);
};
