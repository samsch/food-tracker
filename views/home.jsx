'use strict';

const Html = require('./html');
const Nav = require('./nav');
const Action = require('./action');
const CsrfField = require('./csrf-field');

module.exports = function Home({
	_locals,
	usersFoods,
	lastWeekDailyIntake,
	foodPresets,
}) {
	const user = _locals.req.user;

	return (
		<Html title="Home - Food Tracker">
			<h1>Food Tracker</h1>
			<Nav />
			<p>User: {user.name}</p>
			<h2>Eat something</h2>
			<form action="/eat-food" method="post">
				<CsrfField />
				<div>
					<label htmlFor="food-name">Food name</label>
					<input type="text" id="food-name" name="food_name" />
				</div>
				<div>
					<label htmlFor="food-calories">Calories</label>
					<input type="text" id="food-calories" name="food_calories" />
				</div>
				<div>
					<button type="submit">Add Custom Eaten Food</button>
				</div>
			</form>
			<ul>
				{foodPresets.map(food => {
					return (
						<li>
							<Action
								route="/eat-preset-food"
								value={food.id}
								label={`${food.name}, ${food.calories} Calories`}
							/>
						</li>
					);
				})}
			</ul>
			<h2>
				{"Today's caloric intake so far is "}
				{usersFoods.reduce((calories, food) => calories + food.calories, 0)}
			</h2>
			<h3>Eaten foods</h3>
			<table>
				<thead>
					<tr>
						<th>Food</th>
						<th>Calories</th>
						<th>Eaten At</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{usersFoods.map(item => {
						return (
							<tr>
								<td>{item.name ?? item.food_name}</td>
								<td>{item.calories ?? item.food_calories}</td>
								<td>{item.eaten_at}</td>
								<td>
									<Action
										route="/delete-user-food"
										value={item.id}
										label="Delete Entry"
									/>
									<Action
										route="/duplicate-user-food"
										value={item.id}
										label="Eat Again"
									/>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
			<h2>Daily intake for last week</h2>
			<table>
				<thead>
					<tr>
						<th>Day</th>
						<th>Calories</th>
					</tr>
				</thead>
				<tbody>
					{lastWeekDailyIntake.map(({ day, calories }) => {
						return (
							<tr>
								<td>{day}</td>
								<td>{calories}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
			<h2>Add Food Preset</h2>
			<form action="/add-food-preset" method="post">
				<CsrfField />
				<div>
					<label htmlFor="food-name">Food name</label>
					<input type="text" id="food-name" name="food_name" />
				</div>
				<div>
					<label htmlFor="food-calories">Calories</label>
					<input type="text" id="food-calories" name="food_calories" />
				</div>
				<div>
					<button type="submit">Add Food Preset</button>
				</div>
			</form>
		</Html>
	);
};
