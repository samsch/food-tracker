'use strict';

module.exports = async function eatAtFood(food, { knex }) {
	const calories = parseInt(food.food_calories, 10);
	if (typeof food.food_name !== 'string' || Number.isNaN(calories)) {
		throw new Error(`Food gotta be real. ${JSON.stringify(food)}`);
	}
	return knex('foods').insert({
		name: food.food_name,
		calories,
	});
};
