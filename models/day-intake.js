'use strict';

const { format, parse } = require('date-fns');

function parseDate(input) {
	const date = parse(input, 'yyyy-MM-dd', new Date());
	const dateString = date.format('yyyy-MM-dd');
	if (input !== dateString) {
		throw new Error('Date must be in YYYY-MM-DD format exactly');
	}
	return dateString;
}

async function usersFoods(userId, date, { knex }) {
	const placeholder = date === 'current_date' ? 'current_date' : '?::date';
	const parameters = date === 'current_date' ? [] : [parseDate(date)];
	return knex('users_foods as uf')
		.select('uf.id', 'uf.name', 'uf.calories', 'food_id', 'eaten_at')
		.where({ user_id: userId })
		.whereRaw(
			`(eaten_at at time zone 'EDT')::date = ${placeholder}`,
			parameters,
		)
		.orderBy('eaten_at', 'desc')
		.then(rows => {
			return rows.map(item => {
				// eslint-disable-next-line no-param-reassign
				item.eaten_at = format(item.eaten_at, 'h:mm aaa');
				return item;
			});
		});
}

module.exports = usersFoods;
