'use strict';

const { format } = require('date-fns');

const usersFoods = require('./day-intake');

async function lastWeekDailyIntake(userId, { knex }) {
	return knex
		.raw(
			`
select
	("eaten_at" at time zone 'EDT')::date as "day",
	sum(calories) as "calories"
from "users_foods"
where
  "user_id" = ?
group by ("eaten_at" at time zone 'EDT')::date
order by ("eaten_at" at time zone 'EDT')::date desc
	`,
			[userId],
		)
		.then(({ rows }) => {
			return rows.map(row => {
				// eslint-disable-next-line no-param-reassign
				row.day = format(row.day, 'eee, MMM do');
				return row;
			});
		});
}

async function foodPresets(userId, { knex }) {
	return knex('foods').orderBy('last_used', 'desc').limit(10);
}

async function getAll(userId, { knex }, data) {
	return Promise.all(
		Object.entries(data).map(([name, fetch]) => {
			const p = fetch(userId, { knex });
			return p.then(result => [name, result]);
		}),
	).then(results => {
		return Object.fromEntries(results);
	});
}

async function loadData(user, { knex }) {
	return getAll(
		user.id,
		{ knex },
		{
			usersFoods: (userId, deps) => usersFoods(userId, 'current_date', deps),
			lastWeekDailyIntake,
			foodPresets,
		},
	);
}

module.exports = loadData;
