'use strict';

module.exports = async function eatAtFood(user, food, { knex }) {
	const calories = parseInt(food.food_calories, 10);
	if (typeof food.food_name !== 'string' || Number.isNaN(calories)) {
		throw new Error(`Food gotta be real. ${JSON.stringify(food)}`);
	}
	return knex('users_foods').insert({
		user_id: user.id,
		name: food.food_name,
		calories,
		eaten_at: knex.raw('now()'),
	});
};
