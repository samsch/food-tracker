'use strict';

const { UnauthorizedHTTPException } = require('@samsch/http-errors');

module.exports = {
	requireAuthentication(req, res, next) {
		if (req.user) {
			next();
		} else {
			res.redirect(303, '/login');
		}
	},
	requireAuthenticationApi(req, res, next) {
		if (req.user) {
			next();
		} else {
			throw new UnauthorizedHTTPException();
		}
	},
};
